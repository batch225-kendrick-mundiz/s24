// Activity:
// 1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.
// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.

//template literal
let num = 2
let getCube = Math.pow(num, 3);
console.log(`The cube of ${num} is ${getCube}`);

//array destructuring
const address = ["258", "Washington Ave.", "California", "90011"];

const [number, street, state, postalcode] = address;

console.log(`I live at ${number} ${street} ${state} ${postalcode}`);

//object destructuring
const animal = {
    name: "Luna",
    type: "dog",
    weight: "15 kgs",
    measurement: "1 feet"
}

const {name, type, weight, measurement} = animal;

console.log(`${name} was a ${type}. She weighed at ${weight} with a measurement of ${measurement}`);

//foreach => function

let numArray = [5, 6 ,7, 8, 9, 10];

numArray.forEach(element => {
    console.log(element);
});

//reduce number

const sum = numArray.reduce((accumulator, currentValue) => 
    accumulator + currentValue
);


console.log(sum);


//class
class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const classDog = new dog("Luna", "5", "Beagle");

console.log(classDog);
